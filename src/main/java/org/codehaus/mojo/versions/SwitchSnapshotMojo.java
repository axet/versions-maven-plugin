package org.codehaus.mojo.versions;

import org.apache.maven.plugins.annotations.Mojo;

/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

/**
 * Sets the current projects version, updating the details of any child modules
 * as necessary.
 * 
 * @author Stephen Connolly
 */
@Mojo( name = "switch-snapshot", requiresProject = true, requiresDirectInvocation = true, aggregator = true )
public class SwitchSnapshotMojo extends SwitchMojo {

    @Override
    synchronized String updateSuffix(String groupId, String artifactId, String version) {
        return snapshot(version);
    }

    @Override
    synchronized String incrementModuleVersion(String groupId, String artifactId, String version) {
        return snapshot(incrementVersion(SwitchReleaseMojo.release(version)));
    }

    static public String snapshot(String version) {
        version = version.replaceAll("-SNAPSHOT", "");
        return version + "-SNAPSHOT";
    }
}
