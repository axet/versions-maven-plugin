# versions-maven-plugin
=====================

versions-maven-plugin with methods from/to switch -SNAPSHOT and release versions.

If you tierd of maven release plugin you will find this one very usefull.

Here is two additional commands:

## switch-release

It switch all pom modules, maven dependencies, to the release version (drop -SNAPSHOT suffix) 

    mvn com.github.axet:versions-maven-plugin:switch-release -DnewVersion=$VERSION

## switch-snapshot

Switch all versions to the -SNAPSHOT prefix

    mvn com.github.axet:versions-maven-plugin:switch-snapshot
